class motd {
	file { '/etc/motd': 
		content => '
Beginnings are such delicate times...

				- Frank Herbert, "Dune"

'
	}
}
