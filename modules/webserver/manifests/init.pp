class webserver {

	#################
	#    General    #
	#################

	# Set exec to look for methods in bin and /usr/bin
	Exec {
		path => ['/bin', '/usr/bin']
	}

	exec { 'Apt Update':
		command => 'apt-get update'
	}		

	# Ensure Curl is available
	package { 'curl':
		ensure => present,
		require => Exec['Apt Update'];
	}

	package { 'build-essential':
		ensure => present,
		require => Exec['Apt Update'];
	}

	class { 'ant': }

	class { 'java': 
		jdk => 'true'
	}

	#############################
	##    Message of the Day    #
	#############################

	include motd

	##################
	##    Apache    ##
	##################

	class { "apache": }

	###############
	##    PHP    ##
	###############

	class { 'php': 
		service => 'apache'
	}

	php::module { 'mysql':
		module_prefix => 'php5-'
	}

	php::module { 'gd':
		module_prefix => 'php5-'
	}

	php::pear::module { 'phpqatools':
		repository => 'pear.phpqatools.org',
		use_package => 'no',
		alldeps => 'true'
	}

	php::pecl::module { 'xdebug': }

	class { 'composer': }


	#################
	##    MySQL    ##
	#################

	class { 'mysql': 
		root_password => 'root_password',
		source => 'puppet:///modules/webserver/mysql.conf'
	}

	###############
	##    RVM    ##
	###############

	class { 'rvm': }

	rvm::system_user { ubuntu: ; }

	rvm_system_ruby {
		'ruby-2.0.0-p247':
			ensure => 'present',
			default_use => 'true'
	}

	rvm_gem {
		'rails':
			name => 'rails',
			ensure => present,
			ruby_version => 'ruby-2.0.0-p247',
			require => Rvm_system_ruby['ruby-2.0.0-p247'];
	}

	rvm_gem {
		'jekyll':
			name => 'jekyll',
			ensure => present,
			ruby_version => 'ruby-2.0.0-p247',
			require => Rvm_system_ruby['ruby-2.0.0-p247'];
	}

	rvm_gem {
		'bundler':
			name => 'bundler',
			ensure => present,
			ruby_version => 'ruby-2.0.0-p247',
			require => Rvm_system_ruby['ruby-2.0.0-p247'];
	}

	package { 'libmysql-ruby':
		ensure => present,
		require => Rvm_system_ruby['ruby-2.0.0-p247'];
	}

	package { 'libmysqlclient-dev':
		ensure => present,
		require => Rvm_system_ruby['ruby-2.0.0-p247'];
	}
}
