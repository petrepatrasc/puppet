Puppet Configuration
====================

The repository holds a manifest and dependency injector for setting up a new project with: 

* PHP
* Apache
* MySQL
* Ruby (via RubyGems)
* Ruby on Rails
* Java
* Ant
* Jekyll

All of the other required extensions to make everything work between the software solutions above is also provided.

There also is extended support for a PHP:

* Composer
* Quality Assurance Toolchain
* XDebug
* GD