#!/bin/bash

#########################
##    Forge modules    ##
#########################

# Essential
puppet module install example42/puppi
puppet module install example42/apache
puppet module install example42/php
puppet module install example42/mysql
puppet module install maestrodev/rvm

# General
puppet module install tPl0ch/composer
puppet module install maestrodev/ant
puppet module install example42/java

##########################
##    Custom modules    ##
##########################

if [ ! -d "/etc/puppet/modules/motd" ]; then
	ln -s /var/puppet/modules/motd /etc/puppet/modules/motd
fi

if [ ! -d "/etc/puppet/modules/webserver" ]; then
	ln -s /var/puppet/modules/webserver /etc/puppet/modules/webserver
fi
