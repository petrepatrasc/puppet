#!/bin/bash

# Git and vim
sudo apt-get install -y -qq vim git git-core
echo '# Installed Git and Vim'

# Puppet
wget -q http://apt.puppetlabs.com/puppetlabs-release-stable.deb
sudo dpkg -i puppetlabs-release-stable.deb > install.log
sudo apt-get update -qq
sudo apt-get -y -qq install puppet
rm puppetlabs-release-stable.deb install.log
echo '# Installed Puppet'

# Set up script calls
source scripts.sh
echo '# Set up script helper calls'

# Set up module helpers
source modules.sh
echo '# Loaded up all of the dependand modules'

# Done
echo '# Finished installing dependencies!'
bash
