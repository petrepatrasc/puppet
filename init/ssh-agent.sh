#!/bin/bash

agent_pid="$(ps -ef | grep "ssh-agent" | grep -v "grep" | awk '{print($2)}')"

if [[ -z "$agent_pid" ]]; then
	eval "$(ssh-agent)"
	ssh-add
else
	agent_ppid="$(($agent_pid - 1))"
	agent_sock="$(sudo find /tmp -path "*ssh*" -type s -iname "agent.$agent_ppid")"
	# echo "Agent PID $agent_pid"
	export SSH_AGENT_PID="$agent_pid"
	
	# echo "Agent socket $agent_sock"
	export SSH_AUTH_SOCK="$agent_sock"
fi
